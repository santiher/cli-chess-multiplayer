Super simple cli chess.

Single player: extremely basic ai opponent.  
Multiplayer: same computer and lan.

It is not intended to be fancy, efficient, elegant or to use the best data
structures for chess boards, it's a quick implementation to learn the rules
and play chess on the command line online.

Install and run as:  
* _clichess_local_: single/multiplayer in the same computer on the same console  
* _clichess_server_: start a server for online multiplayer.  
* _clichess_client_: start a client for online multiplayer connected to a
server.

Local usage:
```
usage: clichess_local [-h] [--ai] [--blacks]
                      [--symbols {color_pieces,regular_pieces,unicode_pieces}]

optional arguments:
  -h, --help            show this help message and exit
  --ai                  Play against the ai. Default: false
  --blacks, -b          Play as blacks. Use along --ai. Default: false
  --symbols {color_pieces,regular_pieces,unicode_pieces}
                        Pieces symbols. Default: color_pieces
```
Server usage:
```
usage: clichess_server [-h] [--server SERVER] [--port PORT]

optional arguments:
  -h, --help            show this help message and exit
  --server SERVER, -s SERVER
                        Game server address. Default: localhost
  --port PORT, -p PORT  Game server port. Default: 8091
```

Client usage:
```
usage: clichess_client [-h] [--server SERVER] [--port PORT]

optional arguments:
  -h, --help            show this help message and exit
  --server SERVER, -s SERVER
                        Game server address. Default: localhost
  --port PORT, -p PORT  Game server port. Default: 8091
```


Notation  
The notation used is coordinate notation. E.g.: e2e4  
Castling notation: oo for kingside castling, ooo for queenside castling  
Crowning: add piece symbol at the end of the move. E.g: a7a8N for a knight.
If the crowning piece is not specified, the pawn will be promoted to Queen.
