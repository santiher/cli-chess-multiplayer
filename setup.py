from setuptools import setup


setup(
    name='clichess',
    version='1.0.0',
    packages=['clichess'],
    description='A super simple cli chess',
    author='santiher',
    url='https://gitlab.com/santiher/cli-chess-multiplayer/',
    keywords=['chess', 'cli', 'console'],
    scripts=['scripts/clichess_local', 'scripts/clichess_server',
             'scripts/clichess_client']
)
