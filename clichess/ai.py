import random

from .chess import Chess, GameOver


def evaluate(chess, player):
    """ Score based on weighted amount of pieces. """
    pieces = {1: {k: 0 for k in range(1, 7)}, -1: {k: 0 for k in range(1, 7)}}
    for row in chess.board:
        for cell in row:
            if cell != 0:
                pieces[1 if cell > 0 else -1][abs(cell)] += 1
    score = 0
    for piece_type, piece_value in zip(range(1, 7), (5, 3, 3, 9, 200, 1)):
        score += piece_value * (pieces[1][piece_type] - pieces[-1][piece_type])
    return player * score * random.random()


def negamax(chess, depth, alpha, beta, player):
    if depth == 0:
        return evaluate(chess, player), None
    best_value, best_move = float('-inf'), None
    player_pieces = ((i, j)
                     for i, row in enumerate(chess.board)
                     for j, piece in enumerate(row)
                     if piece != 0 and (piece > 0) == (player > 0))
    for i, j in player_pieces:
        for l in range(8):
            for n in range(8):
                other_chess = chess.copy()
                move = Chess.encode_move(player, [i, j, l, n])
                try:
                    other_chess.apply_move(player, move)
                except GameOver:
                    pass
                except:
                    continue
                n_alpha, n_move = negamax(other_chess, depth-1, -beta,
                                          -alpha, -player)
                n_alpha = -n_alpha
                if n_alpha > best_value:
                    best_value, best_move = n_alpha, move
                alpha = max(alpha, n_alpha)
                if alpha > beta:
                    break
    if best_move is None:
        return evaluate(chess, player), None
    return best_value, best_move


def ai_next_move(chess, player, depth=3):
    value, move = negamax(chess, depth, float('-inf'), float('inf'), player)
    if move is None:
        return 'quit'
    return move
