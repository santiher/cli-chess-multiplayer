#!/usr/bin/env python

import time


class GameOver(BaseException):
    pass


class InvalidMove(BaseException):
    txt = 'Invalid move'


class NotYourTurn(BaseException):
    txt = "It's not your turn!"


class Checked(BaseException):
    pass


class Chess:

    letters = 'abcdefgh'
    numbers = '12345678'
    unicode_pieces = {0: '.',
                      1: '♖', 2: '♘', 3: '♗', 4: '♕',
                      5: '♔', 6: '♙',
                      -1: '♜', -2: '♞', -3: '♝', -4: '♛',
                      -5: '♚', -6: '♟'}
    regular_pieces = {0: '.',
                      1: 'R', 2: 'N', 3: 'B', 4: 'Q',
                      5: 'K', 6: 'P',
                      -1: 'r', -2: 'n', -3: 'b', -4: 'q',
                      -5: 'k', -6: 'p'}
    color_pieces = {player*i: '\033[%dm%s\033[0m' % (color, char)
                    for player, color in ((-1, 92), (1, 91))
                    for i, char in enumerate('RNBQKP', 1)}
    color_pieces[0] = '.'

    def __init__(self):
        self.board = ([[1, 2, 3, 4, 5, 3, 2, 1]] +
                      [[6 for _ in range(8)]] +
                      [[0 for _ in range(8)] for _ in range(4)] +
                      [[-6 for _ in range(8)]] +
                      [[-1, -2, -3, -4, -5, -3, -2, -1]])
        self.current_player = -1
        self.king = {1: [0, 4], -1: [7, 4]}
        self.valid_en_pessant = None
        self.valid_castling = {1: {'oo': True, 'ooo': True},
                               -1: {'oo': True, 'ooo': True}}
        self.start = time.time()
        self.check = False

    def turn(self, player, move):
        if player != self.current_player:
            raise NotYourTurn
        self.is_move_syntax_invalid(player, move)
        backup = self.copy()
        try:
            self.apply_move(player, move)
        except Checked:
            self.restore(backup)
            if self.check_mate(player):
                raise GameOver('Check mate. Player %d wins!' % (-1*player))
            else:
                raise InvalidMove
        self.check = self.in_check(-1*player)
        self.current_player = self.current_player * -1
        if self.check and self.check_mate(-1*player):
            raise GameOver('Check mate. Player %d wins!' % player)

    def read_move(self, player, move):
        self.is_move_syntax_invalid(player, move)
        rmove = [int(move[1]) - 1, ord(move[0]) - 97,
                 int(move[3]) - 1, ord(move[2]) - 97]
        if player == -1:
            rmove[0] = 7 - rmove[0]
            rmove[2] = 7 - rmove[2]
        if len(move) == 5:
            piece = {'R': 1, 'N': 2, 'B': 3, 'Q': 4}[move[4].upper()]
            rmove.append(piece)
        return rmove

    def apply_move(self, player, move):
        if move == 'quit':
            raise GameOver('Player %d surrendered' % player)
        # Castling
        if move in ('oo', 'ooo'):
            if not self.valid_castling[player][move] or self.check:
                raise InvalidMove
            row = 0 if player == 1 else 7
            cols = [1, 2, 3] if 'ooo' else [5, 6]
            for col in cols:
                if self.board[row][col] != 0:
                    raise InvalidMove
            if move == 'oo':
                self.board[row][5] = self.board[row][7]
                self.board[row][6] = self.board[row][4]
                self.board[row][4] = 0
                self.board[row][7] = 0
                self.king[player] = [row, 6]
            else:
                self.board[row][2] = self.board[row][4]
                self.board[row][3] = self.board[row][0]
                self.board[row][4] = 0
                self.board[row][0] = 0
                self.king[player] = [row, 2]
            self.valid_castling[player]['oo'] = False
            self.valid_castling[player]['ooo'] = False
            return
        # Regular moves
        move = self.read_move(player, move)
        piece = self.board[move[0]][move[1]]
        piece_dst = self.board[move[2]][move[3]]
        piece_type = abs(piece)
        vertical_displacement = abs(move[0] - move[2])
        horizontal_displacement = abs(move[1] - move[3])
        valid_en_pessant = self.valid_en_pessant
        self.valid_en_pessant = None
        if piece == 0:
            raise InvalidMove
        if (piece > 0) != (player > 0):
            raise InvalidMove
        if (move[0] == move[2]) and (move[1] == move[3]):
            raise InvalidMove
        if piece_dst != 0 and ((piece > 0) == (piece_dst > 0)):
            raise InvalidMove
        if piece_type == 1:  # Rook
            if (move[0] != move[2]) and (move[1] != move[3]):
                raise InvalidMove
            # Path free
            start_col, start_row, end_col, end_row = move
            if start_col == end_col:
                if start_row > end_row:
                    start_row, end_row = end_row, start_row
                path = ((start_col, i) for i in range(start_row+1, end_row))
            else:
                if start_col > end_col:
                    start_col, end_col = end_col, start_col
                path = ((i, start_row) for i in range(start_col+1, end_col))
            for i, j in path:
                if self.board[i][j] != 0:
                    raise InvalidMove
            if player == 1 and move[0] == 0 and move[1] == 0:
                self.valid_castling[player]['ooo'] = False
            elif player == 1 and move[0] == 0 and move[1] == 7:
                self.valid_castling[player]['oo'] = False
            if player == -1 and move[0] == 7 and move[1] == 0:
                self.valid_castling[player]['ooo'] = False
            elif player == -1 and move[0] == 7 and move[1] == 7:
                self.valid_castling[player]['oo'] = False
        elif piece_type == 2:  # Knight
            if not ((vertical_displacement == 2 and
                     horizontal_displacement == 1) or
                    (vertical_displacement == 1 and
                     horizontal_displacement == 2)):
                raise InvalidMove
        elif piece_type == 3:  # Bishop
            if vertical_displacement != horizontal_displacement:
                raise InvalidMove
            # Path free
            vertical_move = 1 if move[0] < move[2] else -1
            horizontal_move = 1 if move[1] < move[3] else -1
            path = ((i, j) for i, j in zip(
                range(move[0]+vertical_move, move[2], vertical_move),
                range(move[1]+horizontal_move, move[3], horizontal_move)))
            for i, j in path:
                if self.board[i][j] != 0:
                    raise InvalidMove
        elif piece_type == 4:  # Queen
            if ((vertical_displacement != horizontal_displacement) and
                    (move[0] != move[2]) and (move[1] != move[3])):
                raise InvalidMove
            # Path free bishop style
            if vertical_displacement == horizontal_displacement:
                vertical_move = 1 if move[0] < move[2] else -1
                horizontal_move = 1 if move[1] < move[3] else -1
                path = ((i, j) for i, j in zip(
                    range(move[0]+vertical_move, move[2], vertical_move),
                    range(move[1]+horizontal_move, move[3], horizontal_move)))
                for i, j in path:
                    if self.board[i][j] != 0:
                        raise InvalidMove
            else:
                # Path free rook style
                start_col, start_row, end_col, end_row = move
                if start_col == end_col:
                    if start_row > end_row:
                        start_row, end_row = end_row, start_row
                    path = ((start_col, i)
                            for i in range(start_row+1, end_row))
                else:
                    if start_col > end_col:
                        start_col, end_col = end_col, start_col
                    path = ((i, start_row)
                            for i in range(start_col+1, end_col))
                for i, j in path:
                    if self.board[i][j] != 0:
                        raise InvalidMove
        elif piece_type == 5:  # King
            if vertical_displacement > 1 or horizontal_displacement > 1:
                raise InvalidMove
            self.valid_castling[player]['oo'] = False
            self.valid_castling[player]['ooo'] = False
            self.king[player] = [move[2], move[3]]
        elif piece_type == 6:  # Pawn
            starting_position = move[0] == 1 if player == 1 else move[0] == 6
            # Moves backwards
            if ((move[2] > move[0] and player == -1) or
                    (move[2] < move[0] and player == 1)):
                raise InvalidMove
            # Moves two at start
            elif (vertical_displacement == 2 and
                    horizontal_displacement == 0 and
                    starting_position and
                    piece_dst == 0 and
                    self.board[int((move[0]+move[2]) / 2)][move[3]] == 0):
                self.valid_en_pessant = (move[2], move[3])
            # single move forward to empty place
            elif (vertical_displacement == 1 and
                    horizontal_displacement == 0 and
                    self.board[move[2]][move[3]] == 0):
                pass
            # regular eat
            elif (vertical_displacement == 1 and
                  horizontal_displacement == 1 and
                  self.board[move[2]][move[3]] != 0):
                pass
            # en pessant eat
            elif (vertical_displacement == 1 and
                    horizontal_displacement == 1 and
                    valid_en_pessant is not None and
                    valid_en_pessant[0] == move[0] and
                    valid_en_pessant[1] == move[3] and
                    self.board[move[0]][move[3]] != 0):
                self.board[move[0]][move[3]] = 0
            else:
                raise InvalidMove
            # Crowning
            if ((move[2] == 0 and player == -1) or
                    (move[2] == 7 and player == 1)):
                piece = player * (move[4] if len(move) == 5 else 4)
        self.board[move[0]][move[1]] = 0
        self.board[move[2]][move[3]] = piece
        if self.in_check(player):
            raise Checked

    def is_move_syntax_invalid(self, player, move):
        if (move != 'oo' and move != 'ooo' and move != 'quit' and
            (not (len(move) == 4 and
                  (move[0] in self.letters and move[1] in self.numbers and
                   move[2] in self.letters and move[3] in self.numbers))) and
            (not (len(move) == 5 and
                  (move[0] in self.letters and move[1] in self.numbers and
                   move[2] in self.letters and move[3] in self.numbers and
                   (move[4] in 'RBNQ' if len(move) == 5 else True))))):
            raise InvalidMove

    def in_check(self, player):
        king = self.king[player]
        # King attack
        for row in [-1, 0, 1]:
            for col in [-1, 0, 1]:
                i, j = king[0]+row, king[1]+col
                if (i < 0 or i > 7 or j < 0 or j > 7 or
                        self.board[i][j] == 0 or
                        (self.board[i][j] > 0) == (player > 0)):
                    continue
                if abs(self.board[i][j]) in (4, 5):
                    return True
        # Pawn attack
        for row in [1]:
            for col in [1, -1]:
                i, j = king[0]+row*player, king[1]+col
                if (0 <= i <= 7 and 0 <= j <= 7 and
                        abs(self.board[i][j]) in (4, 6) and
                        (self.board[i][j] > 0) != (player > 0)):
                    return True
        # Rook attack
        paths = (zip(range(king[0]-1, -1, -1), (king[1] for _ in range(8))),
                 zip(range(king[0]+1, 8), (king[1] for _ in range(8))),
                 zip((king[0] for _ in range(8)), range(king[1]-1, -1, -1)),
                 zip((king[0] for _ in range(8)), range(king[1]+1, 8)))
        for path in paths:
            for i, j in path:
                piece = self.board[i][j]
                if piece == 0:
                    continue
                elif (piece > 0) == (player > 0) or abs(piece) not in (1, 4):
                    break
                return True
        # Bishop attack
        paths = (zip(range(king[0]-1, -1, -1), range(king[1]-1, -1, -1)),
                 zip(range(king[0]-1, -1, -1), range(king[1]+1, 8)),
                 zip(range(king[0]+1, 8), range(king[1]-1, -1, -1)),
                 zip(range(king[0]+1, 8), range(king[1]+1, 8)))
        for path in paths:
            for i, j in path:
                piece = self.board[i][j]
                if piece == 0:
                    continue
                elif (piece > 0) == (player > 0) or abs(piece) not in (3, 4):
                    break
                return True
        # Knight attack
        rows = [king[0] + i for i in [2, 2, 1, 1, -1, -1, -2, -2]]
        cols = [king[1] + i for i in [1, -1, 2, -2, 2, -2, 1, -1]]
        for i, j in zip(rows, cols):
            if 0 <= i <= 7 and 0 <= j <= 7:
                piece = self.board[i][j]
                if abs(piece) == 2 and (piece > 0) != (player > 0):
                    return True
        return False

    def check_mate(self, player):
        player_pieces = [(i, j)
                         for i, row in enumerate(self.board)
                         for j, piece in enumerate(row)
                         if piece != 0 and (piece > 0) == (player > 0)]
        for i, j in player_pieces:
            for l in range(8):
                for n in range(8):
                    try:
                        other_chess = self.copy()
                        move = Chess.encode_move(player, [i, j, l, n])
                        other_chess.apply_move(player, move)
                        return False
                    except InvalidMove:
                        pass
                    except Checked:
                        pass
        for castling in ('oo', 'ooo'):
            try:
                other_chess = self.copy()
                other_chess.turn(player, castling)
                return False
            except InvalidMove:
                pass
            except Checked:
                pass
        return True

    @staticmethod
    def encode_move(player, move):
        if player == -1:
            move[0] = 7 - move[0]
            move[2] = 7 - move[2]
        move = [Chess.letters[move[1]], Chess.numbers[move[0]],
                Chess.letters[move[3]], Chess.numbers[move[2]]]
        return ''.join(move)

    def to_string(self, player, symbols_type='color_pieces'):
        turn = 'Turn player: %d' % self.current_player
        mins = 'Time: %.2f minutes' % ((time.time() - self.start) / 60.0)
        check = 'Check' if self.check else ''
        cols = '   a b c d e f g h'
        board = self.board if player == -1 else self.board[::-1]
        board = [' '.join(['%d ' % row] +
                 list(map(lambda x: self.piece_number_to_unicode(x,
                                                                 symbols_type),
                          values)) +
                 [' %d' % row])
                 for row, values in zip(range(8, 0, -1), board)]
        board = [turn, mins, check, '', cols, ''] + board + ['', cols]
        ans = '\n'.join(board)
        return ans

    def copy(self):
        c = Chess.__new__(Chess)
        c.board = [list(l) for l in self.board]
        c.current_player = self.current_player
        c.king = {k: list(v) for k, v in self.king.items()}
        c.valid_en_pessant = self.valid_en_pessant
        c.valid_castling = {k1: {k2: v2 for k2, v2 in v1.items()}
                            for k1, v1 in self.valid_castling.items()}
        c.start = self.start
        c.check = self.check
        return c

    def restore(self, other_chess):
        self.board = other_chess.board
        self.current_player = other_chess.current_player
        self.valid_en_pessant = other_chess.valid_en_pessant
        self.valid_castling = other_chess.valid_castling
        self.start = other_chess.start
        self.check = other_chess.check

    def piece_number_to_unicode(self, piece_number, symbols_type):
        return getattr(Chess, symbols_type)[piece_number]

    @staticmethod
    def interactive(player=-1, ai=False, symbols='color_pieces'):
        # Avoid circular import
        if ai:
            from .ai import ai_next_move
        c = Chess()
        while True:
            try:
                if ai:
                    print(c.to_string(player, symbols))
                    if c.current_player == player:
                        c.turn(player, input('> Move: '))
                    else:
                        c.turn(c.current_player,
                               ai_next_move(c, c.current_player))
                else:
                    print(c.to_string(c.current_player, symbols))
                    c.turn(c.current_player, input('> Move: '))
            except InvalidMove:
                print('Invalid move')
            except GameOver as e:
                print('Game over:', str(e))
                break
            except NotYourTurn:
                print('Not your turn')
            except EOFError:
                print('Bye!')
                break
            except KeyboardInterrupt:
                print('Bye!')
                break
