import unittest

from contextlib import contextmanager

from clichess.chess import Chess, InvalidMove, NotYourTurn


empty_chess = Chess()
empty_chess.board = [[0 for _ in range(8)] for _ in range(8)]
empty_chess.king = {1: [-1, -1], -1: [-1, -1]}
empty_chess.valid_castling = {1: {'oo': False, 'ooo': False},
                              -1: {'oo': False, 'ooo': False}}


@contextmanager
def print_assert(chess, player, *txts):
    try:
        yield
    except AssertionError:
        print(chess.to_string(player))
        for txt in txts:
            print(txt)
        raise


class TestChess(unittest.TestCase):

    def test_not_your_turn(self):
        c = empty_chess.copy()
        c.board[4][2] = 6
        with self.assertRaises(NotYourTurn):
            c.turn(1, 'c5c6')

    def test_pawn_regular_moves(self):
        for player, valid_move in ((-1, 'f4f5'), (1, 'f5f6')):
            c = empty_chess.copy()
            c.current_player = player
            c.board[4][5] = player * 6
            for x1 in Chess.letters:
                for x2 in Chess.letters:
                    for y1 in Chess.numbers:
                        for y2 in Chess.numbers:
                            move = '%s%s%s%s' % (x1, y1, x2, y2)
                            if move != valid_move:
                                txt = ('Player %d pawn move: %s' %
                                       (player, move))
                                with print_assert(c, player, txt):
                                    with self.assertRaises(InvalidMove):
                                        c.turn(player, move)
        for player, valid_move in ((-1, 'f4f5'), (1, 'f5f6')):
            c = empty_chess.copy()
            c.current_player = player
            c.board[4][5] = player * 6
            c.turn(player, valid_move)
            txt = 'Player %d pawn move: %s' % (player, valid_move)
            with print_assert(c, player, txt):
                self.assertEqual(c.board[4][5], 0)
                self.assertEqual(c.board[4+player][5], player*6)

    def test_pawn_double_moves(self):
        move = 'e2e4'
        for player, y0, y1 in ((-1, 6, 4), (1, 1, 3)):
            c = Chess()
            c.current_player = player
            c.turn(player, move)
            txt = 'Player %d pawn move: %s' % (player, move)
            with print_assert(c, player, txt):
                self.assertEqual(c.board[y0][4], 0)
                self.assertEqual(c.board[y1][4], player * 6)
        for barrier_piece in list(range(1, 6)) + list(range(-6, 0, -1)):
            for player, y0, y1 in ((-1, 6, 4), (1, 1, 3)):
                c = Chess()
                c.current_player = player
                c.board[int((y1+y0)/2)][4] = barrier_piece
                txt = 'Player %d pawn move: %s' % (player, move)
                with print_assert(c, player, txt):
                    with self.assertRaises(InvalidMove):
                        c.turn(player, move)

    def test_pawn_en_pessant(self):
        pass

    def test_crowning(self):
        pieces_code = {'R': 1, 'N': 2, 'B': 3, 'Q': 4}
        for player, y0, y1 in ((1, 6, 7), (-1, 1, 0)):
            for new_piece in ('N', 'R', 'B', 'Q', ''):
                c = empty_chess.copy()
                c.current_player = player
                c.board[2][3] = 5
                c.board[3][7] = -5
                c.board[y0][0] = player * 6
                move = 'a7a8%s' % new_piece
                txt = 'Player %d pawn crowning move: %s' % (player, move)
                with print_assert(c, player, txt):
                    c.turn(player, move)
                    piece_code = player * pieces_code.get(new_piece, 4)
                    self.assertEqual(c.board[y1][0], piece_code)

    def test_rook_moves(self):
        pass

    def test_bishop_moves(self):
        pass

    def test_knight_moves(self):
        pass

    def test_queen_moves(self):
        pass

    def test_king_moves(self):
        pass

    def test_castling(self):
        c1 = empty_chess.copy()
        c1.board[0][0] = c1.board[0][7] = 1
        c1.board[7][0] = c1.board[7][7] = -1
        c1.board[0][4] = 5
        c1.board[7][4] = -5
        c1.king = {1: [0, 4], -1: [7, 4]}
        c1.valid_castling = {1: {'oo': True, 'ooo': True},
                             -1: {'oo': True, 'ooo': True}}
        for player, i in zip((-1, 1), (7, 0)):
            dst = (6, 2), (5, 3), (7, 0)
            for move, k, r, r0 in zip(('oo', 'ooo'), *dst):
                c2 = c1.copy()
                c2.current_player = player
                c2.turn(player, move)
                with print_assert(c2, player):
                    self.assertEqual(c2.board[i][r0], 0)
                    self.assertEqual(c2.board[i][4], 0)
                    self.assertEqual(c2.board[i][k], player * 5)
                    self.assertEqual(c2.board[i][r], player * 1)
                    self.assertEqual(c2.king[player], [i, k])
                    self.assertFalse(c2.valid_castling[player][move])

    def test_check_by_pawn(self):
        for player, (i, j, k, l) in zip((-1, 1), ((7, 0, 6, 1), (0, 0, 1, 1))):
            c = empty_chess.copy()
            c.current_player = player
            c.king[player] = [i, j]
            c.board[i][j] = player * 5
            c.board[k][l] = player * -6
            with print_assert(c, player):
                self.assertTrue(c.in_check(player))

    def test_check_by_rook(self):
        for player in (-1, 1):
            for i, j in ((4, 0), (4, 7), (0, 4), (7, 4)):
                c = empty_chess.copy()
                c.current_player = player
                c.king[player] = [4, 4]
                c.board[4][4] = player * 5
                c.board[i][j] = player * -1
                with print_assert(c, player):
                    self.assertTrue(c.in_check(player))
        for player in (-1, 1):
            for pawns_player in (-1, 1):
                for i, j, k, l in ((4, 0, 4, 1), (4, 7, 4, 6),
                                   (0, 4, 1, 4), (7, 4, 6, 4)):
                    c = empty_chess.copy()
                    c.current_player = player
                    c.king[player] = [4, 4]
                    c.board[4][4] = player * 5
                    c.board[i][j] = player * -1
                    c.board[k][l] = pawns_player * 6
                    with print_assert(c, player):
                        self.assertFalse(c.in_check(player))

    def test_check_by_bishop(self):
        for player in (-1, 1):
            for i, j in ((2, 2), (2, 6), (6, 6), (6, 2)):
                c = empty_chess.copy()
                c.current_player = player
                c.king[player] = [4, 4]
                c.board[4][4] = player * 5
                c.board[i][j] = player * -3
                with print_assert(c, player):
                    self.assertTrue(c.in_check(player))
        for player in (-1, 1):
            for pawns_player in (-1, 1):
                for i, j, k, l in ((1, 1, 2, 2), (1, 7, 2, 6),
                                   (7, 7, 6, 6), (7, 1, 6, 2)):
                    c = empty_chess.copy()
                    c.current_player = player
                    c.king[player] = [4, 4]
                    c.board[4][4] = player * 5
                    c.board[i][j] = player * -3
                    c.board[k][l] = pawns_player * 6
                    with print_assert(c, player):
                        self.assertFalse(c.in_check(player))

    def test_check_by_knight(self):
        for player in (-1, 1):
            for i, j in ((2, 3), (2, 5), (3, 2), (3, 6),
                         (6, 3), (6, 5), (5, 2), (5, 6)):
                c = empty_chess.copy()
                c.current_player = player
                c.king[player] = [4, 4]
                c.board[4][4] = player * 5
                c.board[i][j] = player * -2
                with print_assert(c, player):
                    self.assertTrue(c.in_check(player))

    def test_check_by_queen(self):
        for player in (-1, 1):
            for i, j in ((2, 2), (2, 6), (6, 6), (6, 2),
                         (4, 0), (4, 7), (0, 4), (7, 4)):
                c = empty_chess.copy()
                c.current_player = player
                c.king[player] = [4, 4]
                c.board[4][4] = player * 5
                c.board[i][j] = player * -4
                with print_assert(c, player):
                    self.assertTrue(c.in_check(player))
        for player in (-1, 1):
            for pawns_player in (-1, 1):
                for i, j, k, l in ((1, 1, 2, 2), (1, 7, 2, 6),
                                   (7, 7, 6, 6), (7, 1, 6, 2),
                                   (4, 0, 4, 1), (4, 7, 4, 6),
                                   (0, 4, 1, 4), (7, 4, 6, 4)):
                    c = empty_chess.copy()
                    c.current_player = player
                    c.king[player] = [4, 4]
                    c.board[4][4] = player * 5
                    c.board[i][j] = player * -4
                    c.board[k][l] = pawns_player * 6
                    with print_assert(c, player):
                        self.assertFalse(c.in_check(player))

    def test_check_by_king(self):
        for player, (i, j, k, l) in zip((-1, 1), ((7, 0, 6, 1), (0, 0, 1, 1))):
            c = empty_chess.copy()
            c.current_player = player
            c.king[player] = [i, j]
            c.board[i][j] = player * 5
            c.board[k][l] = player * -5
            with print_assert(c, player):
                self.assertTrue(c.in_check(player))

    def test_check(self):
        c = Chess()
        c1 = c.board
        c1[0][2] = c1[0][3] = c1[1][2] = c1[7][3] = c1[6][3] = 0
        c1[2][2] = 6
        c1[2][3] = -3
        c1[3][3] = -6
        c1[7][2] = 4
        with print_assert(c, -1):
            self.assertTrue(c.in_check(-1))
        # Other
        c = empty_chess.copy()
        c.current_player = 1
        c.check = True
        c.king = {1: [6, 5], -1: [0, 4]}
        b = c.board
        b[0][0] = -1
        b[0][1] = -2
        b[0][2] = -3
        b[0][4] = -5
        b[1][3] = -6
        b[1][5] = -6
        b[1][6] = -6
        b[2][2] = 1
        b[2][4] = -6
        b[2][5] = -2
        b[2][6] = -1
        b[3][1] = -6
        b[3][2] = -3
        b[3][7] = -6
        b[4][1] = 6
        b[5][2] = 6
        b[5][3] = 6
        b[5][4] = 6
        b[5][5] = 6
        b[5][7] = 6
        b[6][0] = -4
        b[6][5] = 5
        b[6][6] = 6
        b[7][1] = 2
        b[7][4] = 4
        b[7][5] = 3
        b[7][6] = 2
        b[7][7] = 1
        with print_assert(c, 1):
            self.assertTrue(c.in_check(1))

    def test_check_make(self):
        c = empty_chess.copy()
        c.current_player = -1
        c.board[7][0] = -5
        c.king[-1] = [7, 0]
        self.assertFalse(c.check_mate(-1))
        c.board[0][0] = 4
        c.board[7][7] = 4
        c.board[6][1] = 6
        c.board[5][2] = 6
        c.board[4][4] = -6
        with print_assert(c, -1):
            self.assertTrue(c.check_mate(-1))
        # Other
        c = Chess()
        c1 = c.board
        c1[0][2] = c1[0][3] = c1[1][2] = c1[7][3] = c1[6][3] = 0
        c1[2][2] = 6
        c1[2][3] = -3
        c1[3][3] = -6
        c1[7][2] = 4
        with print_assert(c, -1):
            self.assertTrue(c.check_mate(-1))
        # Other
        c = empty_chess.copy()
        c.current_player = 1
        c.check = True
        c.king = {1: [3, 7], -1: [7, 4]}
        c.board[0][6] = 2
        c.board[0][7] = 1
        c.board[1][4] = -2
        c.board[2][5] = -2
        c.board[2][7] = 6
        c.board[3][0] = -1
        c.board[3][6] = 6
        c.board[3][7] = 5
        c.board[4][1] = -6
        c.board[5][3] = -6
        c.board[5][5] = 6
        c.board[5][6] = -6
        c.board[7][2] = -3
        c.board[7][4] = -5
        with print_assert(c, 1):
            self.assertFalse(c.check_mate(1))
        # Other
        c = empty_chess.copy()
        c.current_player = 1
        c.check = True
        c.king = {1: [1, 7], -1: [5, 5]}
        c.board[1][4] = -2
        c.board[1][7] = 5
        c.board[2][5] = -3
        c.board[3][6] = -6
        c.board[4][1] = -6
        c.board[5][3] = -6
        c.board[6][5] = -5
        with print_assert(c, 1):
            self.assertTrue(c.check_mate(1))

    def test_check_mate_unchanged(self):
        c1 = empty_chess.copy()
        c1.current_player = -1
        c1.board[7][0] = -5
        c1.king[-1] = [7, 0]
        c2 = c1.copy()
        c1.check_mate(-1)
        self.assertEqual(c1.board, c2.board)
        c1 = empty_chess.copy()
        c1.current_player = -1
        c1.board[7][0] = -5
        c1.king[-1] = [7, 0]
        c1.board[0][0] = 4
        c1.board[7][7] = 4
        c1.board[6][1] = 6
        c1.board[5][2] = 6
        c1.board[4][4] = -6
        c2 = c1.copy()
        c1.check_mate(-1)
        self.assertEqual(c1.board, c2.board)


if __name__ == '__main__':
    unittest.main()
